package be.kdg.stack;

import java.util.ArrayList;
import java.util.List;

/**
 * Maak de klasse generiek, zodat ze kan gebruikt worden voor elk type.
 * Werk de methoden push, pop en top uit.
 */
public class LimitedStack {
    private final int capacity;
    private int count;
    private List<Object> elements;

    public LimitedStack() {
        this(10); // default stack size
    }

    public LimitedStack(int size) {
        this.capacity = size > 0 ? size : 10;
        count = 0; // stack is aanvankelijk leeg
        elements = new ArrayList<>(size);
    }

    /**
     * Maak de methode generic. Voeg het element toe aan de stack, maar test eerst op
     * de capaciteit en doe een eventuele throw van een FullStackException.
     */
    public void push(Object nieuw) {

    }

    /**
     * Maak de methode generic. Neem het meest recente element weg, maar test eerst op
     * het aantal en doe een eventuele throw van een EmptyStackException.
     */
    public Object pop() {

        return null;
    }

    /**
     * Maak de methode generic. Retourneer het meest recente element, maar test eerst op
     * het aantal en doe een eventuele throw van een EmptyStackException.
     */
    public Object top() {

        return null;
    }

    public int capacity() {
        return capacity;
    }

    public int size() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < capacity; i++) {
            if (i < count) {
                sb.append(elements.get(i).toString());
            } else {
                sb.append("?");
            }
            if (i < capacity - 1) {
                sb.append(", ");
            } else {
                sb.append("]");
            }
        }
        return sb.toString();
    }
}
